import './style/main.css'
import adblockDetection from 'theme-library/module/adblock-detection'
import dataLink from 'theme-library/module/data-link'
import dataSearch from 'theme-library/module/data-search'
import dataToggle from 'theme-library/module/data-toggle'

const head = document.querySelector('head')
const body = document.querySelector('body')
window.addEventListener('load', () => adblockDetection(head, body))
body.addEventListener('click', (event) => {
  dataLink(event)
  dataToggle(event)
})
body.addEventListener('submit', dataSearch)
