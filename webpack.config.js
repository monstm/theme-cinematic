const fs = require('fs')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { LoremIpsum } = require('lorem-ipsum')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')

const isProduction = !!process.env.CI_COMMIT_TAG
// const isProduction = true

const imageAssets = ['.webp', '.svg', '.png', '.jpg', '.jpeg', '.gif']
const fontAssets = ['.ttf', '.eot', '.woff', '.woff2']
const assetResources = [].concat(imageAssets, fontAssets)

const sourceDirectory = path.resolve(__dirname, 'src')
const distributionDirectory = path.resolve(__dirname, 'dist')

const getHtmlPlugins = (sourceDirectory) => {
  const htmlPlugins = []
  const twigDirectory = path.resolve(sourceDirectory, 'twig')

  const files = fs.readdirSync(twigDirectory)
  const twigFiles = files.filter(file => path.extname(file).toLowerCase() === '.twig')

  const movies = movieFactory(100)

  const templateParameters = {
    isProduction,
    theme: {
      name: 'Theme Cinematic',
      link: 'https://gitlab.com/monstm/theme-cinematic'
    },
    movie: movies[0],
    onGoingMovies: getOngoingMovies(movies),
    mostViewedMovies: getMostViewedMovies(movies),
    topRatedMovies: getTopRatedMovies(movies)
  }

  twigFiles.forEach(twigFile => {
    const twigName = path.parse(twigFile).name
    const template = path.resolve(twigDirectory, twigFile)
    const filename = `html/${twigName}.html`

    htmlPlugins.push(new HtmlWebpackPlugin({
      template,
      filename,
      templateParameters
    }))
  })

  return htmlPlugins
}

const randomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const randomFloat = (min, max, decimals) => {
  return Math.random() * (max - min) + min
}

const movieFactory = (size) => {
  const movies = []
  const lorem = new LoremIpsum()

  for (let index = 0; index < size; index++) {
    const rate = randomFloat(3, 9)
    const view = randomInt(1, 300)
    const release = randomInt(2000, 2023)

    movies.push({
      title: lorem.generateWords(randomInt(3, 5)),
      overview: lorem.generateParagraphs(5),
      rate: rate.toFixed(1),
      _rate: rate,
      view: `${view}k+`,
      _view: view,
      release: release.toString(),
      _release: release,
      poster: `https://source.unsplash.com/2000x3000?movie&${index}`,
      backdrop: `https://source.unsplash.com/1422x800?movie&${index}`
    })
  }

  return movies
}

const getOngoingMovies = (movies) => {
  return movies.sort(function (a, b) {
    if (a._release < b._release) return 1
    if (a._release > b._release) return -1
    return 0
  })
}

const getMostViewedMovies = (movies) => {
  return movies.sort(function (a, b) {
    if (a._view < b._view) return 1
    if (a._view > b._view) return -1
    return 0
  })
}

const getTopRatedMovies = (movies) => {
  return movies.sort(function (a, b) {
    if (a._rate < b._rate) return 1
    if (a._rate > b._rate) return -1
    return 0
  })
}

const hasExtension = (filename, extentions) => {
  const extname = path.extname(filename)
  const extension = extname.split('?')[0]
  return extentions.includes(extension)
}

let mode = 'production'
let outputFilename = 'main.js'
let cssFilename = 'main.css'
let assetFilename = '[name][ext]'

if (!isProduction) {
  mode = 'development'
  outputFilename = 'main-[contenthash].js'
  cssFilename = 'main-[contenthash].css'
  assetFilename = '[name]-[contenthash][ext]'
}

const plugins = [
  new MiniCssExtractPlugin({
    filename: cssFilename
  })
].concat(getHtmlPlugins(sourceDirectory))

module.exports = {
  mode,
  entry: './src/entry.js',
  output: {
    path: distributionDirectory,
    filename: outputFilename,
    assetModuleFilename: (params) => {
      let assetDiretory = 'asset'

      if (hasExtension(params.filename, imageAssets)) {
        assetDiretory = 'image'
      }

      if (hasExtension(params.filename, fontAssets)) {
        assetDiretory = 'font'
      }

      return `${assetDiretory}/${assetFilename}`
    },
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader'
          // 'sass-loader',
        ]
      },
      {
        test: /\.twig$/,
        use: ['twig-loader']
      },
      {
        test: new RegExp(assetResources.join('|') + '$', 'i'),
        type: 'asset/resource'
      }
    ]
  },
  plugins,
  devServer: {
    static: {
      directory: distributionDirectory
    },
    compress: true,
    port: 9000
  },
  resolve: {
    fallback: {
      fs: false
    }
  },
  devtool: false
}
