# Theme Cinematic

## Pages

* [Index Page](https://monstm.gitlab.io/theme-cinematic/html/index.html)
* [List Page](https://monstm.gitlab.io/theme-cinematic/html/list-page.html)
* [Single Page](https://monstm.gitlab.io/theme-cinematic/html/single-page.html)
* [Form Page](https://monstm.gitlab.io/theme-cinematic/html/form-page.html)
* [Error Document](https://monstm.gitlab.io/theme-cinematic/html/404.html)
